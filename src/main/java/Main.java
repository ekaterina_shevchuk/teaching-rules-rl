import java.io.IOException;

public class Main {
    public static void main(String[] args){
        GetMorphemes getMorphemes = new GetMorphemes();
        String word = "Пригодный";
        try {
            Word wordMorph = getMorphemes.get(word);
            wordMorph.print();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
