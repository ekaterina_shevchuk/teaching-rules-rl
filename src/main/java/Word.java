public class Word {
    private String prefix;
    private String root;
    private String suffix;
    private String ending;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getEnding() {
        return ending;
    }

    public void setEnding(String ending) {
        this.ending = ending;
    }

    public void print(){
        System.out.println(prefix);
        System.out.println(root);
        System.out.println(suffix);
        System.out.println(ending);
    }
}
