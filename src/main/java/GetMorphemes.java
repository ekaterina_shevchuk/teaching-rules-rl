import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class GetMorphemes {

    public Word get(String word) throws IOException {
        Word wordMorphemes = new Word();
        String searchQuery = word.substring(0, 1).toUpperCase() + "/" + firstLowerCase(word);
        String baseUrl = "https://morphemeonline.ru/" ;
        String url = baseUrl+searchQuery;
        Document doc = Jsoup.connect(url).get();
        Elements prefixs = doc.select("[class=\"based\"]"); // there must be a "prefix", but the html code of this site has a peculiarity
        for (Element prefix : prefixs) {
            wordMorphemes.setPrefix(prefix.text());
        }
        Elements roots = doc.select("[class=\"root\"]");
        for (Element root : roots) {
            wordMorphemes.setRoot(root.text());
        }
        Elements suffixs = doc.select("[class=\"suffix\"]");
        for (Element suffix : suffixs) {
            wordMorphemes.setSuffix(suffix.text());
        }
        Elements endings = doc.select("[class=\"ending\"]");
        for (Element ending : endings) {
            wordMorphemes.setEnding(ending.text());
        }
        return wordMorphemes;
    }

    private String firstLowerCase(String word){
        if(word == null || word.length()==0) return "";
        return word.substring(0, 1).toLowerCase() + word.substring(1);
    }

}
